# wasi-polyfills usage example

This repo provides an example on basic usage of WASI polyfills.

Read more on [my blog](https://twdev.blog/2023/11/wasm_cpp_04/).

## Building

```bash
source wasmenv.sh

./build.sh

npm install

npx parcel src/index.html
```

Inspect JavaScript console to see the output.

### Prerequisites

You need [WASI-SDK](https://github.com/WebAssembly/wasi-sdk) installed under
/opt.  `wasmenv.sh` assumes version 20 used - this might have to be adjusted if
your version differs.
