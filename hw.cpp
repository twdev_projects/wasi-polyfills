#include <iostream>

int main(int argc, const char *argv[]) {
  std::cout << "argc: " << argc << std::endl;

  for (std::size_t i = 0; i < argc; ++i) {
    std::cout << "argv[" << i << "] -> " << argv[i] << std::endl;
  }

  std::cout << "hello world" << std::endl;
  return 0;
}
