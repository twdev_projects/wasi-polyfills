import wasm_module from 'url:./hw.wasm';

import {
  File,
  OpenFile,
  PreopenDirectory,
  WASI
} from "../node_modules/@bjorn3/browser_wasi_shim/dist";

let stdin = new OpenFile(new File([]));
let stdout = new OpenFile(new File([]));
let stderr = new OpenFile(new File([]));

let args = [ "hw", "arg1", "arg2" ];
let env = [ "FOO=bar" ];
let fds = [ stdin, stdout, stderr ];

let wasi = new WASI(args, env, fds);

WebAssembly
    .instantiateStreaming(fetch(wasm_module), {
      "wasi_snapshot_preview1" : wasi.wasiImport,
    })
    .then((obj) => {
      wasi.start(obj.instance);

      let d = new TextDecoder().decode(stdout.file.data);
      console.log(d);
    });
